/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  // admin1 超级管理员 admin 租户管理员
  const valid_map = ['admin', 'editor','admin1']
  return valid_map.indexOf(str.trim()) >= 0
}
