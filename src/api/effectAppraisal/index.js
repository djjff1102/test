import request from '@/utils/request'
import exportRequest from '@/utils/exportRequest'
export default {
    /************************ 能源统计报表 *************************** */
    // 报表数据获取
    EnergyStatisticsReports:(params)=>{
        return request({
            url: '/eneCompareStandardManagement/showAndQuery-eneEnergyStatisticsReports',
            method: 'get',
            timeout: 60000,
            params
        })
    }
    // 报表数据导出
    ,exportReports:(params)=>{
        return exportRequest({
            url: '/eneCompareStandardManagement/export-eneEnergyStatisticsReports',
            method: 'get',
            params,
            responseType: "blob",
        })
    }

    /************************ 能效评估 *************************** */
    // 能效评估-获取
    ,EnergyCompareStandardManagementCost: (params)=>{
        return request({
            url: '/eneCompareStandardManagement/showAndQuery-eneEnergyCompareStandardManagementCost',
            method: 'get',
            params
        })
    }
    // 填写成本 查询&展示
    ,eneCostShowAndQuery:(data)=>{
        return request({
            url: '/eneCost/showAndQuery-eneCost',
            method: 'post',
            data
        })
    }
    // 填写成本 保存
    ,eneCostSave: (data)=>{
        return request({
            url: '/eneCost/save-eneCost',
            method: 'post',
            data
        })
    }

    /************************ 节能管理 *************************** */
    // 昨日用量
    ,EnergyLastEnergyConsumeStatisticsGet:(params)=>{
        return request({
            url: '/eneCompareStandardManagement/showAndQuery-eneEnergyLastEnergyConsumeStatistics',
            method: 'get',
            params,
            timeout: 100000 //请求较慢
        })
    }
    // 上月用量
    ,EnergyLastMonthEnergyConsumeStatisticsGet: (params)=>{
        return request({
            url: '/eneCompareStandardManagement/showAndQuery-eneEnergyLastMonthEnergyConsumeStatistics',
            method: 'get',
            params
        })
    }
    // 能源成本
    ,EnergySavingManagementCost: (params)=>{
        return request({
            url: '/eneCompareStandardManagement/showAndQuery-eneEnergySavingManagementCost',
            method: 'get',
            params
        })
    }
    // 能源汇总
    ,EnergySavingManagementEnergySummary: (params)=>{
        return request({
            url: '/eneCompareStandardManagement/showAndQuery-eneEnergySavingManagementEnergySummary',
            method: 'get',
            params
        })
    }
    // 所有能源类型获取
    ,getEnergyTypeList: ()=>{
        return request({
            url: '/ene/eneDataWindows/getConsumeIndices',
            method: 'get'
        })
    }
    

}