import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/ene/web/system/query-by-token',
    method: 'get',
    
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}
export function Password(data) {
  return request({
    url: '/ene/web/eneUser/update-password',
    method: 'post',
    data
  })
}
