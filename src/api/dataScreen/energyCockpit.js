import request from '@/utils/request'

export function getQuartConsume(bcId) {
  // 季度消耗量
  return request({
    url: '/ene/web/consume/quartConsume',
    method: 'get',
    params: {
      bcId
    }
  })
}
export function getWeekConsume(bcId) {
  // 本周消耗量
  return request({
    url: '/ene/web/consume/weekConsume',
    method: 'get',
    params: {
      bcId
    }
  })
}

export function getMonthConsume(bcId) {
  // 本月消耗量
  return request({
    url: '/ene/web/consume/monthConsume',
    method: 'get',
    params: {
      bcId
    }
  })
}

export function getDayConsume(bcId) {
  // 当日消耗量
  return request({
    url: '/ene/web/consume/dayConsume',
    method: 'get',
    params: {
      bcId
    }
  })
}

export function getWindowsWarningInformationLists(params) {
  // 各车间设备实时预警列表
  return request({
    url: '/ene/eneDataWindows/showAndQuery-windowsWarningInformationLists',
    method: 'get',
    params
  })
}


export function getConsumeIndices(params) {
  // 数据大屏 - 获取能源折标下拉框数据列表
  return request({
    url: '/ene/eneDataWindows/getConsumeIndices',
    method: 'get',
    params
  })
}



export function getwindowsEachWorkshopConsumeStatistics(timeDimension, energyTypeId) {
  // 数据大屏 - 制药生产线各车间能源消耗情况
  return request({
    url: '/ene/eneDataWindows/showAndQuery-windowsEachWorkshopConsumeStatistics',
    method: 'get',
    params: {
      timeDimension,
      energyTypeId
    }
  })
}

export function getwindowsEachEquipmentLists(timeDimension, workshopId, energyTypeId) {
  // 数据大屏 - 设备能源使用情况统计 - 设备排行列表
  return request({
    url: '/ene/eneDataWindows/showAndQuery-windowsEachEquipmentLists',
    method: 'get',
    params: {
      timeDimension,
      workshopId,
      energyTypeId

    }
  })
}


// export function getwindowsEachEquipmentDetails(params) {
//   // 数据大屏 - 设备能源使用情况统计 - 单个设备能耗详情
//   return request({
//     url: '/ene/eneDataWindows/showAndQuery-windowsEachEquipmentDetails',
//     method: 'get',
//     params
//   })
// }

export function getallMonthConsume(bcId) {
  // 数据大屏 - 设备能源使用情况统计 - 各月份能源统计对比
  return request({
    url: '/ene/web/consume/allMonthConsume',
    method: 'get',
    params: {
      bcId
    }
  })
}

export function getwindowsEachEquipmentSingle(timeDimension, equipmentId) {
  // 数据大屏 - 设备能源使用情况统计 - 单个设备能耗详情
  return request({
    url: '/ene/eneDataWindows/showAndQuery-windowsEachEquipmentDetails',
    method: 'get',
    params: {
      timeDimension,
      equipmentId
    }
  })
}
