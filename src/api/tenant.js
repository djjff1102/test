import request from '@/utils/request'

// export function login(data) {
//   return request({
//     url: '/login',
//     method: 'post',
//     data
//   })
// }

// export function getInfo() {
//   return request({
//     url: '/ene/web/system/query-by-token',
//     method: 'get',

//   })
// }

// export function logout() {
//   return request({
//     url: '/logout',
//     method: 'post'
//   })
// }

export default {
  getTenantList(data) {
    return request({
      url: '/ene/web/eneTenant/query-web-psyArchives',
      method: 'post',
      data
    })
  },
  addTenant(data) {
    return request({
      url: '/ene/web/eneTenant/add',
      method: 'post',
      data
    })
  },
  editTenant(data) {
    return request({
      url: '/ene/web/eneTenant/update',
      method: 'post',
      data
    })
  },
  delTenant(id) {
    return request({
      url: `/ene/web/eneTenant/delete/${id}`,
      method: 'post',
    })
  }
}
