import request from '@/utils/request'
import exportRequest from '@/utils/exportRequest'
// 单元管理--查询页面展示和模糊查询
export function getshow(data) {
  return request({
    url: '/ene/eneUnitManagement/showAndQueryLikeNoPage',
    method: 'post',
    data

  })
}
// 单元管理--修改状态
export function getupdate(data) {
  return request({
    url: '/ene/eneUnitManagement/updateSelect',
    method: 'post',
    data,
    timeout:100000

  })
}
// 单元管理--修改每个设备对象选择状态
export function getselect(data) {
  return request({
    url: '/ene/eneUnitManagement/updateSelectOne',
    method: 'post',
    data

  })
}





// 单元--用量同比环比的
export function getList(data) {
  return request({
    url: '/ene/unitStatistic/unitYoYMoM/getUsedBarEcharts',
    method: 'post',
    data,
    timeout:100000

  })
}
// 单元--拆标同比环比的
export function getList2(data) {
  return request({
    url: '/ene/unitStatistic/unitYoYMoM/getFactorBarEcharts',
    method: 'post',
    data,
    timeout:100000

  })
}
// 单元--能源指标
export function getenergy(params) {
  return request({
    url:'/ene/unitStatistic/unitYoYMoM/getAllEnergyTypes',
    method:'get',
    params,
    timeout:100000
  })
}
// 单元趋势折线图
export function getmyline(data){
  return request({
    url:'/ene/unitStatistic/unitTrend/getTrendLineEcharts',
    method:'post',
    data,
    timeout:100000
  })
}
// 设备的饼图
export function getmypie(data){
  return request({
     url:'/ene/unitStatistic/unitProportion/getEquipmentEchartsPie',
      method:'post',
      data,
      timeout:100000
  })
 
}
// 车间的饼图
export function getmypie2(data){
  return request({
     url:'/ene/unitStatistic/unitProportion/getWorkshopEchartsPie',
      method:'post',
      data,
      timeout:100000
  })
}
//设备排行
export function getequipment(data){
  return request({
     url:'/ene/unitStatistic/unitProportion/getEquipmentTopFive',
      method:'post',
      data,
      timeout:100000
  })
}
//车间排行
export function getworkshop(data){
  return request({
     url:'/ene/unitStatistic/unitProportion/getWorkshopTopFive',
      method:'post',
      data,
      timeout:100000
  })
}
// 单元同比环比表格
export function gettable(data){
  console.log(data);
  return request({
     url:'/ene/unitStatistic/unitYoYMoM/getYoyMomTable',
      method:'post',
      data,
      timeout:100000
  })
 
}

// 单元趋势表格
export function getunit(data){
  console.log(data);
  return request({
     url:'/ene/unitStatistic/unitTrend/getUnitTrendTable',
      method:'post',
      data,
      timeout:100000
  })
 
}

//单元同比环比导出
export function exp(params) {
  return exportRequest({
    url:'/ene/unitStatistic/unitYoYMoM/export',
    // GET /ene/unitStatistic/unitTrend/export
    methods:'get',
    params:{
      dateParam:params
    },
    timeout:100000,
    responseType: 'blob'
  })
}
//单元趋势导出
export function exps(params) {
  return exportRequest({
    url:'/ene/unitStatistic/unitTrend/export',
    methods:'get',
    params:{
      dateParam:params
    },
    timeout:100000,
    responseType: 'blob'
  })
}
//累积量展示的柱状图
export function getworkbar(params) {
  return request({
    url:`/ene/unitStatistic/getWorkshopConsumptionBar`,
    method:'get',
    params,
    timeout:100000
  })
}
//累积量展示的柱状图
export function getworkpie(params) {
  return request({
    url:`/ene/unitStatistic/getEnergyConsumptionPie`,
    method:'get',
    params,
    timeout:100000
  })
}