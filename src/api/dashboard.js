import request from '@/utils/request'
import exportRequest from '@/utils/exportRequest'

export default {
  /* ********************流程图  *********************** */
  // 获取能源列表
  getEnergyList() {
    return request({
      url: '/ene/eneWorkshop/show-basicCofigDownlistDatas',
    })
  },
  // 获取设备列表
  getDeviceList() {
    return request({
      url: '/ene/eneEquipment/show-allEquipmentsDownLists',
    })
  },
  // 添加设备
  addADevice(data) {
    return request({
      url: '/ene/eneEquipment/set-equipmentInformation',
      method: 'post',
      data
    })
  },
  // 获取能源配置的连线种类
  getConnectionType(data) {
    return request({
      url: '/ene/eneEquipment/show-getLines',
    })
  },
  // 车间列表
  getWorkshopList(data) {
    return request({
      url: '/ene/eneWorkshop/show-currentTenantWorkshop',
      method:'post',
      data
    })
  },
  // 车间列表
  editWorkshopList(data) {
    return request({
      url: '/ene/eneWorkshop/update-eneWorkshop',
      method:'post',
      data
    })
  },
  // 车间列表
  delWorkshopList(id) {
    return request({
      url: `/ene/eneWorkshop/delete-eneWorkshop/${id}`,
    })
  },
  addWorkshopList(data) {
    return request({
      url: `/ene/eneWorkshop/save-eneWorkshop`,
      method:'post',
      data
    })
  },

  // 删除设备
  deleteDevice(data) {
    return request({
      url: `/ene/eneEquipment/delete-equipmentInformation`,
      method:'post',
      data
    })
  },
  // 设置总览图表名称
  setAnOverviewChartName(data) {
    return request({
      url: `/ene/eneEquipment/modified-workshopChartOneName`,
      method:'post',
      data
    })
  },
  // 获取总览图表名称
  getAnOverviewChartName(id) {
    return request({
      url: `/ene/eneEquipment/show-workshopChartOneName/${id}`,
      method:'get',
    })
  },
  // 更改设备
  changeDevice(data) {
    return request({
      url: `/ene/eneEquipment/update-equipmentInformation`,
      method:'post',
      data
    })
  },
  // 获取当前车间设备预警状态
  getAWarningState(id) {
    return request({
      url: `/ene/eneEquipment/showAndQuery-workshopWarningInformationWindows/${id}`,
    })
  },

  /* ********************基础配置*********************** */
  flowchartSave(data) {
    return request({
      url: '/ene/eneEquipment/linked-eneEquipmentFlows',
      method: 'post',
      data
    })
  },
  getFlowChart(data) {
    return request({
      url: '/ene/eneEquipment/showAndQuery-eneEquipmentFlows',
      method: 'post',
      data,
      timeout:0
    })
  },
  // 获取车间
  getEnePlanningAWorkshopList() {
    return request({
      url: '/ene/enePlanning/show-workshopDownlistData',
    })
  },
  /* ********************预警信息*********************** */
  getWarningInformationList(data) {
    return request({
      url: '/ene/eneWarningInformation/showAndQuery-warningInformation',
      method: 'post',
      data
    })
  },
  // 获取车间
  getAWorkshopList() {
    return request({
      url: '/ene/eneWorkshop/show-workshopDownlistDatas',
    })
  },
  // 获取指标
  getTargetList(id) {
    return request({
      url: '/ene/eneWorkshop/show-basicCofigDownlistDatas?queryType='+id,
    })
  },
  //导出
  exportAlertInformation(data) {
    return exportRequest({
      url: '/ene/eneWarningInformation/export-warningInformation',
      method: 'post',
      data,
      responseType: 'blob'
    })
  },

  /* ********************预警设置*********************** */

  getWarningSettingsList(data) {
    return request({
      url: '/ene/eneWarningInformation/showAndQuery-warningConfiguration',
      method: 'post',
      data
    })
  },
  addAWarningSettings(data) {
    return request({
      url: '/ene/eneWarningInformation/save-warningConfiguration',
      method: 'post',
      data
    })
  },
  editAWarningSettings(data) {
    return request({
      url: '/ene/eneWarningInformation/update-warningConfiguration',
      method: 'post',
      data
    })
  },
  delAWarningSettings(id) {
    return request({
      url: `/ene/eneWarningInformation/delete-warningConfiguration/${id}`,
   

    })
  },
  // 获取预警设备
  getAnEarlyWarningDevice() {
    return request({
      url: '/ene/eneWarningInformation/show-selectedWorkshopEquipment',
    })
  },
  getAWarningDeviceDetails(id) {
    return request({
      url: `/ene/eneWarningInformation/show-warningConfigurationDetail/${id}`,
    })
  },
  /* ********************指标查询*********************** */
  getIndicatorQuery(data) {
    return request({
      url: `/ene/eneWarningInformation/queryAndShow-indexQuery`,
      method:'post',
      data
    })
  },
  // 获取车间
  getWorkshop(id) {
    return request({
      url: `/ene/eneWarningInformation/show-selectedWorkshopEquipmentDownLists/${id}`,
    })
  },
  //导出
  exportIndicatorQuery(data) {
    return exportRequest({
      url: '/ene/eneWarningInformation/export-indexQuery',
      method: 'post',
      data,
      responseType: 'blob'
    })
  },

  /* ********************趋势曲线*********************** */

  getTrendCurve(data) {
    return request({
      url: `/ene/eneWarningInformation/queryAndShow-trendQuery`,
      method:'post',
      data,
      timeout:0
    })
  }
}
  