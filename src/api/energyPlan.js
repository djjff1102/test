import request from '@/utils/request'

export function getCreatorList(params) {
  // 计划制定-获取计划创建人列表
  return request({
    url: '/ene/enePlanning/show-creatorDownlistData',
    method: 'get',
    params
  })
}
export function getPlanList(creatorId, planDate, current, size) {
  // 计划制定-查询
  return request({
    url: '/ene/enePlanning/showAndQuery-planning',
    method: 'post',
    data: {
      creatorId,
      planDate,
      current,
      size
    }
  })
}
export function addEnergyPlanList(addPlanTime) {
  // 计划制定-获取新增能源计划列表
  return request({
    url: '/ene/enePlanning/showAndQuery-planningDetail',
    method: 'post',
    data: {
      planningDate: addPlanTime
    }
  })
}
export function addEnergyPlan(detailsData, planDate) {
  // 计划制定-新增能源计划
  return request({
    url: '/ene/enePlanning/save-planning',
    method: 'post',
    data: {
      detailsData,
      planDate
    }
  })
}
export function editEnergyPlan(detailsData, planDate) {
  // 计划制定-编辑能源计划
  return request({
    url: '/ene/enePlanning/update-planning',
    method: 'post',
    data: {
      detailsData,
      planningDate: planDate
    }
  })
}
export function getPlantList() {
  // 计划制定-获取车间列表
  return request({
    url: '/ene/enePlanning/show-workshopDownlistData',
    method: 'get',
  })
}
export function getEnergyTypeList() {
  // 计划制定-获取能源类型
  return request({
    url: '/ene/eneWorkshop/show-basicCofigDownlistDatas',
    method: 'get',
    params: {
      queryType: 1
    }
  })
}
export function getchartData(planDate, energyType) {
  // 计划分析-图表
  return request({
    url: '/ene/enePlanning/showAndQuery-planningStatisticsAnalysisChart',
    method: 'get',
    params: {
      planDate,
      energyType
    }
  })
}
export function getPlanTraceList(planningDate, current, size) {
  // 计划跟踪-查询
  return request({
    url: '/ene/enePlanning/showAndQuery-planningTracking',
    method: 'post',
    data: {
      planningDate,
      current,
      size

    }
  })
}
export function getReportOne(planDate) {
  // 计划分析-计划报告-全厂同比
  return request({
    url: '/ene/enePlanning/report-one',
    method: 'get',
    timeout: 600000,
    params: {
      planDate,

    }
  })
}

export function getReportTow(planDate) {
  // 计划分析-计划报告
  return request({
    url: '/ene/enePlanning/report-two',
    method: 'get',
    timeout: 600000,
    params: {
      planDate,

    }
  })
}

export function getReportThree(planDate) {
  // 计划分析-计划报告
  return request({
    url: '/ene/enePlanning/report-three',
    method: 'get',
    timeout: 600000,
    params: {
      planDate,

    }
  })
}
export function getReportFour(planDate) {
  // 计划分析-计划报告
  return request({
    url: '/ene/enePlanning/report-four',
    method: 'get',
    timeout: 600000,
    params: {
      planDate,

    }
  })
}

export function deleteplan(id) {
  // 计划跟踪-查询
  return request({
    url: '/enePlanningDetails/delete/' + id,
    method: 'post',
  })
}
