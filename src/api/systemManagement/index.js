import request from '@/utils/request'
export default{
      /* ********************租户管理*********************** */
    // 租户管理-获取
    getTenantList: (data)=>{
        return request({
            url: '/ene/web/eneTenant/query-web-psyArchives',
            method: 'post',
            data
        })
    },
    // 租户管理-修改
    editTenant: (data)=>{
        return request({
            url: '/ene/web/eneTenant/update',
            method: 'post',
            data
        })
    },
    // 租户管理-新增
    addTenant: (data)=>{
        return request({
            url: '/ene/web/eneTenant/add',
            method: 'post',
            data
        })
    },
    // 租户管理-删除
    delTenant: (id)=>{
        return request({
            url: `/ene/web/eneTenant/delete/${id}`,
            method: 'post'
        })
    },
    // 租户管理-删除
    tenantStatus: (data)=>{
        return request({
            url: `/ene/web/eneTenant/edit-tenantStatus`,
            method: 'post',
            data
        })
    }

      /* ********************基础配置*********************** */
    // 基础配置-获取
    ,getConfigList: (data)=>{
        return request({
            url: '/ene/eneBasicConfig/showAndQuery',
            method: 'post',
            data
        })
    }
    // 基础配置-删除
    ,delConfig:(id)=>{
        return request({
            url: `/ene/eneBasicConfig/delete/${id}`,
            method: 'post'
        })
    }
    // 基础配置-修改
    ,updateConfig:(data)=>{
        return request({
            url: '/ene/eneBasicConfig/update',
            method: 'post',
            data
        })
    }
    // 基础配置-新增
    ,addConfig:(data)=>{
        return request({
            url: '/ene/eneBasicConfig/add',
            method: 'post',
            data
        })
    }
    // 基础配置-获取全部能源指标
    ,getAllIndices:(data)=>{
        return request({
            url: '/ene/eneBasicConfig/getAllIndices',
            method: 'post',
            data
        })
    }

      /* ********************用户管理*********************** */
    // 用户管理-获取
    ,getUserList:(data)=>{
        return request({
            url: '/ene/web/eneUser/query-by-param',
            method: 'post',
            data
        })
    }
    // 用户管理-修改
    ,editUser:(data)=>{
        return request({
            url: '/ene/web/eneUser/update',
            method: 'post',
            data
        })
    }
    // 用户管理-新增
    ,addUser:(data)=>{
        return request({
            url: '/ene/web/eneUser/add',
            method: 'post',
            data
        })
    }
    // 用户管理-删除
    ,delUser:(id)=>{
        return request({
            url: `/ene/web/eneUser/delete/${id}`,
            method: 'post'
        })
    }
    // 用户管理-重置密码
    ,resetUserPass: (id)=>{
        return request({
            url: '/ene/web/eneUser/reset-password',
            method: 'post',
            params: {
                id
            }
        })
    }

    /* ********************操作日志*********************** */    
    // 操作日志-获取
    ,getLogList:(data)=>{
        return request({
            url: '/ene/web/eneOplog/query-by-page',
            method: 'post',
            data
        })
    }
    ,getTenantName:()=>{
        return request({
            url: '/ene/web/eneUser/get-tenantName',
            method: 'get'
        })
    }
} 