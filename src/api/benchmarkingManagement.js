import request from '@/utils/request'

export default {
  // 获取图表
  getChart(params) {
    return request({
      url: '/eneCompareStandardManagement/showAndQuery-eneEnergyCompareStandardManagement',
      params
    })
  },
  // 获取表格
  getTableList(params) {
    return request({
      url: '/eneCompareStandardManagement/showAndQuery-eneEnergyCompareStandardManagementForm',
      params
    })
  },
  // 获取信息
  gettingInformation(params) {
    return request({
      url: '/eneCompareStandardManagement/showAndQuery-eneEnergyCompareStandardManagementSelectedOneChart',
      params,
      timeout:0
    })
  },

}