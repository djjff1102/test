import request from '@/utils/request'

export default {
  // 获取设备
  getEquipmentList(data) {
    return request({
      url: '/ene/eneEquipment/showAndQuery-eneEquipmentManagement',
      method: 'post',
      data
    })
  },
  // 获取车间下拉列表
  getWorkshopDropDownList(data) {
    return request({
      url: '/ene/eneWorkshop/show-workshopDownlistDatas',
    })
  },
  // 删除设备
  deleteDevice(id) {
    return request({
      url: `/ene/eneEquipment/delete-eneEquipmentManagement/${id}`,
    })
  },
  // 添加设备
  addADevice(data) {
    return request({
      url: `/ene/eneEquipment/save-eneEquipmentManagement`,
      data,
      method: 'post',
    })
  },
  // 编辑设备
  editADevice(data) {
    return request({
      url: `/ene/eneEquipment/update-eneEquipmentManagement`,
      data,
      method: 'post',
    })
  },
   // 获取指标
   getTargetList() {
    return request({
      url: '/ene/eneWorkshop/show-basicCofigDownlistDatas',
    })
  },
   // 获取传感器
   getTheSensorList(data) {
    return request({
      url: '/ene/eneEquipment/show-eneEquipmentManagementSensorsByEquipmentIdDown',
      method: "post",
      data
    })
  },
   // 删除传感器
   deleteSensor(id) {
    return request({
      url: `/ene/eneEquipment/delete-eneEquipmentManagementSensor/${id}`,
    })
  },
}