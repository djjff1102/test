import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css' // lang i18n

import '@/styles/font/font.scss';
import '@/styles/index.scss' // global css
import '@/assets/font/iconfont.css'

import App from './App'
import store from './store'
import router from './router'

import Highcharts from 'highcharts/highstock';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsDrilldown from 'highcharts/modules/drilldown';
import Highcharts3D from 'highcharts/highcharts-3d';

HighchartsMore(Highcharts)
HighchartsDrilldown(Highcharts);
Highcharts3D(Highcharts);

import '@/icons' // icon
import '@/permission' // permission control

import permission from '@/permission/index'
Vue.use(permission)

import baseLayout from "@/components/baseLayout/up-center-down-container";
Vue.component('base-layout', baseLayout)

// 不用全局引入，在每个组件里面，局部引入，更方便
// import echarts from 'echarts'
// Vue.prototype.echarts = echarts


import 'vue-super-flow/lib/index.css'
// import SuperFlow from 'vue-super-flow'
// Vue.use(SuperFlow)

import SuperFlow from './components/super-flow/index.vue'
Vue.use(SuperFlow)

// loading
function startLoading(target) {
    this.$nextTick(()=>{
        if (!Vue.prototype.$currentLoading) {
            let load = Vue.prototype.$loading({
              target: document.querySelector(`.${target}`),
              lock: true,
              text: '数据加载中...',
              spinner: '',
              customClass: 'custom-class'
            })
            Vue.prototype.$currentLoading = load
          }      
    })
    
  }
  function closeLoading() {
    if (Vue.prototype.$currentLoading) {
      Vue.prototype.$currentLoading.close()
      Vue.prototype.$currentLoading = null
    }
  }
  Vue.prototype.$startLoading = startLoading
  Vue.prototype.$closeLoading = closeLoading
  Vue.prototype.$currentLoading = null

// 尝试修改为多个loading /节能效果评估/节能管理 主要使用
// loading
function startLoading_d(target) {
    // if(!Vue.prototype.$currentLoading_d.has(target)){
        this.$nextTick(()=>{
          let load = Vue.prototype.$loading({
            target: document.querySelector(`.${target}`),
            lock: true,
            text: '数据加载中...',
            spinner: '',
            customClass: 'custom-class'
          })
          // if(!Vue.prototype.$currentLoading_d.has(target)){
            Vue.prototype.$currentLoading_d.set(target,{target: target,currentLoading: load})
          // }
    })
  // }
}
function closeLoading_d(target) {
  // console.log(Vue.prototype.$currentLoading_d)
  // if (Vue.prototype.$currentLoading_d.size!=0&&Vue.prototype.$currentLoading_d.has(target)) {
    Vue.prototype.$currentLoading_d.get(target).currentLoading.close()
    // Vue.prototype.$currentLoading_d.clear(target)
  // }
}
Vue.prototype.$startLoading_d = startLoading_d
Vue.prototype.$closeLoading_d = closeLoading_d
Vue.prototype.$currentLoading_d = new Map()

Vue.directive('throttle', {
  bind: (el, binding) => {
    let throttleTime = binding.value; // 防抖时间
    if (!throttleTime) { // 用户若不设置防抖时间，则默认2s
      throttleTime = 2000;
    }
    let cbFun;
    el.addEventListener('click', event => {
      if (!cbFun) { // 第一次执行
        cbFun = setTimeout(() => {
          cbFun = null;
        }, throttleTime);
      } else {
        event && event.stopImmediatePropagation();
      }
    }, true);
  },
});




// set ElementUI lang to EN
Vue.use(ElementUI)
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

const vm = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})

export default vm
