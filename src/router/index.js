import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },




]

export const asyncRoutes = [

  {
    path: '/',
    component: Layout,
    redirect: '/productionOverview',
    meta: {
      title: '生产监控',
      icon: "iconfont icon-icon_1shengchanjiankongbeifen",
      roles: ['2', '1']
    },
    children: [{
      path: 'productionOverview',
      name: 'productionOverview',
      component: () => import('@/views/dashboard/productionOverview.vue'),
      meta: {
        title: '生产总览',
        roles: ['2', '1']
      }
    }, {
      path: 'basicConfiguration',
      name: 'basicConfiguration',
      component: () => import('@/views/dashboard/basicConfiguration.vue'),
      meta: {
        title: '生产流程图',
        activeMenu: '/productionOverview',
        roles: ['1']
      },
      hidden: true
    }, {
      path: 'energyFlowMap',
      name: 'energyFlowMap',
      component: () => import('@/views/dashboard/energyFlowMap'),
      meta: {
        title: '能源流向图',
        roles: ['2', '1']
      }
    }, {
      path: 'warningInformation',
      name: 'warningInformation',
      component: () => import('@/views/dashboard/warningInformation.vue'),
      meta: {
        title: '预警信息',
        roles: ['2', '1']
      }
    }, {
      path: 'warningSettings',
      name: 'warningSettings',
      component: () => import('@/views/dashboard/warningSettings.vue'),
      meta: {
        title: '预警设置',
        roles: ['2', '1']
      }
    }, {
      path: 'indicatorQuery',
      name: 'indicatorQuery',
      component: () => import('@/views/dashboard/indicatorQuery.vue'),
      meta: {
        title: '指标查询',
        roles: ['2', '1']
      }
    }, {
      path: 'trendCurve',
      name: 'trendCurve',
      component: () => import('@/views/dashboard/trendCurve'),
      meta: {
        title: '趋势曲线',
        roles: ['2', '1']
      }
    }]

  },
  {
    path: '/energyProgram',
    component: Layout,
    redirect: '/energyProgram',
    meta: {
      title: '能源计划',
      icon: "iconfont icon-icon_2nengyuanjihua",
      roles: ['2', '1']
    },
    children: [{
      path: 'index',
      name: 'basicConfiguration',
      component: () => import('@/views/energyProgram/planFormulate/index.vue'),
      meta: {
        title: '计划制定',
      }
    }, {
      path: 'planTrace',
      name: 'basicConfiguration',
      component: () => import('@/views/energyProgram/planTrace/index.vue'),
      meta: {
        title: '计划跟踪',
      }
    }, {
      path: 'statistics',
      name: 'basicConfiguration',
      component: () => import('@/views/energyProgram/statistics/index.vue'),
      meta: {
        title: '统计分析',
      }
    }]

  },
  {
    path: '/effectAppraisal',
    component: Layout,
    redirect: '/effectAppraisal/index',
    meta: { title: '节能效果评估', icon: "iconfont icon-icon_3jienengxiaoguopinggubeifen", roles: ['2','1'] },
    children: [{
        path: 'index',
        name: 'energyAppraisal',
        component: () => import('@/views/effectAppraisal/energyAppraisal.vue'),
        meta: { title: '能效评估', }
    },{
        path: 'statement',
        name: 'statement',
        component: () => import('@/views/effectAppraisal/statement.vue'),
        meta: {title: '能源统计报表'}
    },{
        path: 'benchmarkingManagement',
        name: 'benchmarkingManagement',
        component: () => import('@/views/effectAppraisal/benchmarkingManagement.vue'),
        meta: {title: '对标管理'}
    },{
        path: 'energyManagement',
        name: 'energyManagement',
        component: () => import('@/views/effectAppraisal/energyManagement.vue'),
        meta: {title: '节能管理'}
    }
    ]
  },
  {
    path: '/energAnalyse',
    component: Layout,
    redirect: '/energAnalyse/un',
    meta: { title: '节能监控分析', icon: "iconfont icon-icon_4jienengjiankongbeifen", roles: ['2','1'] },
    children:[
        { path: 'un',
          name: 'un',
          component: () => import('@/views/energAnalyse/unitManger/un.vue'),
          meta: { title: '单元管理', }

        },
        {
        path: 'index',
            name: 'index',
            component: () => import('@/views/energAnalyse/info/index.vue'),
            meta: { title: '统计分析', },
            
        },
       
        
    ]
},
  {
    path: '/systemManagement',
    component: Layout,
    redirect: '/systemManagement/basicSet',
    meta: {
      title: '系统管理',
      icon: "iconfont icon-icon_6xitongguanlibeifen"
    },
    children: [{
      path: 'index',
      name: 'basicConfiguration',
      component: () => import('@/views/systemManagement/index.vue'),
      meta: {
        title: '租户管理',
        roles: ['0']
      }
    }, {
      path: 'basicSet',
      name: 'basicSet',
      component: () => import('@/views/systemManagement/basicSet.vue'),
      meta: {
        title: '基础配置',
        roles: ['2', '1']
      }
    }, {
      path: 'usersManagement',
      name: 'usersManagement',
      component: () => import('@/views/systemManagement/usersManagement.vue'),
      meta: {
        title: '用户管理',
        roles: ['0', '1']
      }
    }, {
      path: 'operationLog',
      name: 'operationLog',
      component: () => import('@/views/systemManagement/operationLog.vue'),
      meta: {
        title: '操作日志',
        roles: ['0', '1']
      }
    }, {
      path: 'deviceManagement',
      name: 'deviceManagement',
      component: () => import('@/views/systemManagement/deviceManagement.vue'),
      meta: {
        title: '设备管理',
        roles: ['2', '1']
      }
    }]

  },
  {
    path: '/dataScreen',
    component: Layout,
    redirect: '/dataScreen/index',
    alwaysShow: true, // 就是这行显隐起作用的
    meta: {
      title: '大屏可视化',
      icon: "iconfont icon-icon_5dapingkeshihuabeifen",
      roles: ['2', '1']
    },
    children: [{
        path: 'energy-cockpit',
        name: 'energy-ockpit',
        component: () => import('@/views/dataScreen/energyCockpit.vue'),
        meta: {
          title: '能源驾驶舱'
        },
      },

    ]
  },
  {
    path: '/equipment-management-one',
    component: () => import('@/views/dataScreen/energyCockpitParticulars.vue'),
    redirect: '/dataScreen',
    hidden: true,
    children: [{
      path: '',
      name: 'equipment-management-one',
      component: () => import('@/views/dataScreen/energyCockpitParticulars.vue'),
      meta: {
        title: '能耗消耗设备管理大屏1',
      },
      hidden: true //有两个以上时删除该句

    }]
  },
  // 404 page must be placed at the end !!!
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
