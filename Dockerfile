FROM nginx

COPY dist/ /opt/energy_supervision_web/

COPY nginx/default.conf /etc/nginx/conf.d/default.conf